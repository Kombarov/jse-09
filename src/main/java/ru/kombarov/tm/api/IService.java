package ru.kombarov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IService<T> {

    @NotNull
    List<T> findAll();

    @NotNull
    List<T> findAll(final @Nullable String userId) throws Exception;

    @Nullable
    T findOne(final @Nullable String id);

    @Nullable
    T findOne(final @Nullable String userId,final @Nullable String id) throws Exception;

    void persist(final @Nullable T t) throws Exception;

    void merge(final @Nullable T t) throws Exception;

    void remove(final @Nullable String id) throws Exception;

    void removeAll();

    void removeAll(final @Nullable String userId) throws Exception;

    @Nullable
    String getIdByName(final @Nullable String name) throws Exception;
}
