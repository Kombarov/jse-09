package ru.kombarov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.service.ProjectService;
import ru.kombarov.tm.service.TaskService;
import ru.kombarov.tm.service.UserService;

public interface ServiceLocator {

    @NotNull
    ProjectService getProjectService();

    @NotNull
    TaskService getTaskService();

    @NotNull
    UserService getUserService();
}
