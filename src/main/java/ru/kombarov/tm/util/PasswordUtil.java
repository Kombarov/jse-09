package ru.kombarov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class PasswordUtil {

    @Nullable
    public static String generateHash(final @Nullable String password) throws NoSuchAlgorithmException {
        if (password == null || password.isEmpty()) return null;
        final @NotNull MessageDigest md = MessageDigest.getInstance("MD5");
        final @NotNull byte[] digest = md.digest(password.getBytes());
        final @NotNull String hash = new String(digest);
        return hash;
    }
}
