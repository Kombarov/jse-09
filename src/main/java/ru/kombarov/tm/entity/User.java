package ru.kombarov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractEntity {

    private @Nullable String login;

    private @Nullable String password;

    private @Nullable Role role;

    public User(final @NotNull Role role) {
        this.role = role;
    }
}
