package ru.kombarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.enumerated.Status;
import ru.kombarov.tm.util.EntityUtil;

public class TaskListByStatusCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-list show by status";
    }

    @NotNull
    @Override
    public String description() {
        return "Show tasks by status.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST BY STATUS]");
        System.out.println(Status.PLANNED.displayName() + ":");
        if (serviceLocator == null) throw new Exception();
        if (serviceLocator.getUserService().getUserCurrent() == null) throw new Exception();
        EntityUtil.printTasks(serviceLocator.getTaskService().sortByStatus(Status.PLANNED, serviceLocator.getUserService().getUserCurrent().getId()));
        System.out.println("\n" + Status.DONE.displayName() + ":");
        EntityUtil.printTasks(serviceLocator.getTaskService().sortByStatus(Status.DONE, serviceLocator.getUserService().getUserCurrent().getId()));
        System.out.println("\n" + Status.IN_PROCESS.displayName() + ":");
        EntityUtil.printTasks(serviceLocator.getTaskService().sortByStatus(Status.IN_PROCESS, serviceLocator.getUserService().getUserCurrent().getId()));
        System.out.println("[OK]");
    }
}
