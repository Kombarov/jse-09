package ru.kombarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

public final class UserLogoutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-logout";
    }

    @NotNull
    @Override
    public String description() {
        return "Logout.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("USER LOG OUT");
        if (serviceLocator == null) throw new Exception();
        serviceLocator.getUserService().setUserCurrent(null);
        System.out.println("[OK]");
    }
}
