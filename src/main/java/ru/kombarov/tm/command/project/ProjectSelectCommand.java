package ru.kombarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.printProject;
import static ru.kombarov.tm.util.EntityUtil.printProjects;

public final class ProjectSelectCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-select";
    }

    @NotNull
    @Override
    public String description() {
        return "Select the project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT SELECT]");
        if (serviceLocator == null) throw new Exception();
        if (serviceLocator.getUserService().getUserCurrent() == null) throw new Exception();
        printProjects(serviceLocator.getProjectService().findAll(serviceLocator.getUserService().getUserCurrent().getId()));
        System.out.println("ENTER PROJECT NAME");
        printProject(serviceLocator.getProjectService().findOne(serviceLocator.getProjectService().getIdByName(input.readLine())));
        System.out.println("[OK]");
    }
}
