package ru.kombarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.IService;
import ru.kombarov.tm.entity.Task;
import ru.kombarov.tm.enumerated.Status;
import ru.kombarov.tm.repository.AbstractRepository;
import ru.kombarov.tm.repository.TaskRepository;

import java.util.List;

public final class TaskService extends AbstractService<Task> implements IService<Task> {

    @NotNull
    private final TaskRepository taskRepository = (TaskRepository) abstractRepository;

    public TaskService(AbstractRepository<Task> abstractRepository) {
        super(abstractRepository);
    }

    @Nullable
    @Override
    public String getIdByName(final @Nullable String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        return abstractRepository.getIdByName(name);
    }

    @NotNull
    public List<Task> getTasksByProjectId(final @Nullable String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new Exception();
        return taskRepository.getTasksByProjectId(projectId);
    }

    @NotNull
    public List<Task> getTasksByUserId(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        return taskRepository.getTasksByUserId(userId);
    }

    @NotNull
    @Override
    public List<Task> findAll(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        return abstractRepository.findAll();
    }

    @Nullable
    @Override
    public Task findOne(final @Nullable String userId, final @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return abstractRepository.findOne(userId, id);
    }

    @Override
    public void removeAll(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        abstractRepository.removeAll(userId);
    }

    @NotNull
    public List<Task> sortByDateStart(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        return taskRepository.sortByDateStart(userId);
    }

    @NotNull
    public List<Task> sortByDateFinish(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        return taskRepository.sortByDateFinish(userId);
    }

    @NotNull
    public List<Task> sortByStatus(final @NotNull Status status, final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        return taskRepository.sortByStatus(status, userId);
    }

    @NotNull
    public List<Task> findByPart(final @Nullable String part, final @Nullable String userId) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (userId == null || userId.isEmpty()) throw new Exception();
        return taskRepository.findByPart(part, userId);
    }
}