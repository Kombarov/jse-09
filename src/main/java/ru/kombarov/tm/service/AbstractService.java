package ru.kombarov.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.IService;
import ru.kombarov.tm.entity.AbstractEntity;
import ru.kombarov.tm.repository.AbstractRepository;

import java.util.List;

@AllArgsConstructor
public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    protected @NotNull AbstractRepository<T> abstractRepository;

    @Override
    public void persist(final @Nullable T t) throws Exception {
        if (t == null) throw new Exception();
        else abstractRepository.persist(t);
    }

    @Override
    public void merge(final @Nullable T t) throws Exception {
        if (t == null) throw new Exception();
        else abstractRepository.merge(t);
    }

    @NotNull
    @Override
    public List<T> findAll() {
        return abstractRepository.findAll();
    }

    @Nullable
    @Override
    public T findOne(final @Nullable String id) {
        if (id == null || id.isEmpty()) return null;
        else return abstractRepository.findOne(id);
    }

    @Override
    public void remove(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        else abstractRepository.remove(id);
    }

    @Override
    public void removeAll() {
        abstractRepository.removeAll();
    }
}
